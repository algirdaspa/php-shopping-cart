<?php

namespace InvertusTask\ShoppingCart\models;

use function InvertusTask\ShoppingCart\utils\currencyConverter;
use function InvertusTask\ShoppingCart\utils\productIndexSearch;

class Cart {
  private $products;
  private $totalCartSum;
  private $defaultCurrency;

  function __construct($defaultCurrency) {
    $this->defaultCurrency = $defaultCurrency;
    $this->totalCartSum = 0;
    $this->products = array();
  }

  private function updateTotalCartSum() {
    $tempCartSum = 0;
    
    foreach ($this->products as $product) {
      $tempCartSum += currencyConverter($this->defaultCurrency, $product->currency, $product->price) * $product->quantity;
    }

    $this->totalCartSum = $tempCartSum;
  }

  function addToCart($productToAdd) {
    $productIndex = productIndexSearch($this->products, $productToAdd);

    // If the product does not exist, we should create a new item in the product array, otherwise, we update the existing item
    if ($productIndex === -1) {
      array_push($this->products, $productToAdd);
    } else {
      $this->products[$productIndex]->quantity += $productToAdd->quantity;
    }
    
    $this->updateTotalCartSum();
  }

  function removeFromCart($productToRemove) {
    $productIndex = productIndexSearch($this->products, $productToRemove);

    if ($productIndex === -1) return;
    $foundProductQuantity = $this->products[$productIndex]->quantity;

    // If the quantity after removal is less than or equals to zero, we remove the item from the product array, otherwise, we update the quantity 
    if ($foundProductQuantity >= $foundProductQuantity - abs($productToRemove->quantity) && $foundProductQuantity - abs($productToRemove->quantity) > 0) {
      $this->products[$productIndex]->quantity -= abs($productToRemove->quantity);
    } else {
      array_splice($this->products, $productIndex, 1);
    }
    
    $this->updateTotalCartSum();
  }

  function getTotalItems() {
    $totalItems = 0;
    
    foreach ($this->products as $product) {
      $totalItems += $product->quantity;
    }

    return $totalItems;
  }

  function getTotalSum() {
    return $this->totalCartSum;
  }

  function getDefaultCurrency() {
    return $this->defaultCurrency;
  }

  function returnCart() {
    return "(" . $this->getTotalItems() . ") " . $this->getTotalSum() . " " . $this->getDefaultCurrency();
  }
}