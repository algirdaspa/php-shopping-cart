<?php

namespace InvertusTask\ShoppingCart\models;

class Product {
  public $id;
  public $title;
  public $quantity;
  public $price;
  public $currency;

  function __construct($id, $title, $quantity, $price, $currency) {
    $this->id = $id;
    $this->title = $title;
    $this->quantity = $quantity;
    $this->price = $price;
    $this->currency = $currency;
  }
}
