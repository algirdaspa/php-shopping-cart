<?php


namespace InvertusTask\ShoppingCart\utils;

use function InvertusTask\ShoppingCart\utils\exchangeRatesParser;

function currencyConverter ($targetCurrency, $productCurrency, $price, $exchangeRatesFile = __DIR__ . "/../data/exchangeRates.txt") {
  $exchangeRates = exchangeRatesParser($exchangeRatesFile);
  $convertedPrice = $price * $exchangeRates[$productCurrency][$targetCurrency];
  
  return number_format((float) $convertedPrice, 2, '.', '');
}