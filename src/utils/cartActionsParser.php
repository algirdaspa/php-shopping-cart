<?php

namespace InvertusTask\ShoppingCart\utils;

function cartActionsParser ($filePath) {
  // In the while loop, we loop trought this array in order to assign keys to the parsed values for easier later use
  $actionKeys = array("id", "title", "quantity", "price", "currency");
  $actions = array();

  if ($file = fopen($filePath, "r")) {
    while(!feof($file)) {
      $explodedAction = explode(";", fgets($file));
      $parsedAction = array();

      /*
      * If the size of exploded action array is less than 6, it means that the file is either empty
      * or the format of action in the current line of the loop is incorect. In this case, the incorect
      * value is skipped. If the file is empty, loop simply finishes. Exception is not thrown because the
      * program can operate even if the file is empty or contains incorect product values.
      */

      if (sizeof($explodedAction) !== 6) continue;

      foreach ($actionKeys as $index => $actionKey) {
        $parsedAction[$actionKey] = $explodedAction[$index];
      }

      $parsedAction["operation"] = $parsedAction["quantity"] > 0 ? "add" : "remove";
      array_push($actions, $parsedAction);
    }
    
    fclose($file);
  }

  return $actions;
}