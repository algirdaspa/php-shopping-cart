<?php

namespace InvertusTask\ShoppingCart\utils;

function exchangeRatesParser ($filePath) {
  $exchangeRates = array();

  if ($file = fopen($filePath, "r")) {
    while(!feof($file)) {
      $singleCurrencyExchangeRates = array();
      $explodedValues = explode(";", fgets($file));
      array_pop($explodedValues);

      // If the size of exploded values at any time exuals to 0, it either indicates that the file is empty or a blank line is left in the file
      if (sizeof($explodedValues) === 0) throw new \Exception('Empty exchange rates file or incorrect format!');
     
      foreach ($explodedValues as $index => $value) {
        
        // According to the exchangeRates.txt structure, first currency in a line is a host currency. Therefore, we grab the key for later use
        if ($index === 0) $currencyKey = $value;

        // According to the exchangeRates.txt structure, if the value of index + 1 is odd, we are dealing with currency title e.g. (EUR) 
        if (($index + 1) % 2 === 1) {

          // If key currency does not have a value pair, it means that the format is incorrect
          if (!isset($explodedValues[$index + 1])) throw new \Exception('Incorrect exchange rates format!');
          
          $singleCurrencyExchangeRates[$value] = $explodedValues[$index + 1];
        }
      }
     
      $exchangeRates[$currencyKey] = $singleCurrencyExchangeRates;
    }

    fclose($file);
  }

  // If one of the currencies does not have an exchange rate for all the provided currencies it indicates a format error
  foreach ($exchangeRates as $index => $value) {
    if (sizeof($exchangeRates) !== sizeof($exchangeRates[$index])) throw new \Exception('Incorrect exchange rates format!');
  }
  
  return $exchangeRates;
}
