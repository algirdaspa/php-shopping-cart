<?php

namespace InvertusTask\ShoppingCart\utils;

/* 
* Returns -1 if item has not been found, otherwise, returns index of an item.
* As the user has an ability to add the same product in different currencies,
* We consider a product to be found if its id and the currency is the same.
*/

function productIndexSearch ($products, $item) {
  foreach ($products as $index => $product) {
    if ($product->id === $item->id && $product->currency === $item->currency) {
      return $index;
    }
  }

  return -1;
}