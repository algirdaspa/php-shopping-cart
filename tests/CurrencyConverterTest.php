<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use function InvertusTask\ShoppingCart\utils\currencyConverter;

class CurrencyConverterTest extends TestCase {
  function testCurrencyConversionOperation() {
    $expectedResult = 140.79;
    $actualResult = currencyConverter("EUR", "USD", 159.99, __DIR__ . "/data/testExchangeRatesFullOperation.txt");

    $this->assertEquals($expectedResult, $actualResult);
  }
}