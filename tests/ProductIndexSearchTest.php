<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use InvertusTask\ShoppingCart\models\Product;

use function InvertusTask\ShoppingCart\utils\productIndexSearch;

class ProductIndexSearchTest extends TestCase {
  function testProductIndexSearchProductExist() {
    $products = array(
      new Product("mbp", "MacBook Pro", 2, 2.99, "EUR"),
      new Product("mbp", "MacBook Pro", 5, 100.09, "GBP")
    );

    $product = new Product("mbp", "MacBook Pro", 5, 100.09, "GBP");
    
    $expectedResult = 1;
    $actualResult = productIndexSearch($products, $product);

    $this->assertEquals($expectedResult, $actualResult);
  }

  function testProductIndexSearchProductDoesNotExist() {
    $products = array(
      new Product("mbp", "MacBook Pro", 2, 2.99, "EUR"),
      new Product("mbp", "MacBook Pro", 5, 100.09, "USD")
    );

    $product = new Product("mbp", "MacBook Pro", 5, 100.09, "GBP");
    
    $expectedResult = -1;
    $actualResult = productIndexSearch($products, $product);

    $this->assertEquals($expectedResult, $actualResult);
  }
}