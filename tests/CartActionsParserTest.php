<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use function InvertusTask\ShoppingCart\utils\cartActionsParser;

class CartActionsParserTest extends TestCase {
  function testCartActionsEmptyFile() {
    $expectedResult = array();
    $actualResult = cartActionsParser(__DIR__ . "/data/testCartActionsEmpty.txt");
    
    $this->assertEquals($expectedResult, $actualResult);
  }

  function testCartActionsIncorrectFormat() {
    $expectedResult = array(
      array(
        "id" => "mbp",
        "title"  => "MacBook Pro",
        "quantity" => 2,
        "price" => 29.99,
        "currency" => "EUR",
        "operation" => "add"
      ),
      array(
        "id" => "mbp",
        "title"  => "MacBook Pro",
        "quantity" => 5,
        "price" => 100.09,
        "currency" => "GBP",
        "operation" => "add"
      ),
    );

    $actualResult = cartActionsParser(__DIR__ . "/data/testCartActionsIncorectFormat.txt");
    
    $this->assertEquals($expectedResult, $actualResult);
  }

  function testCartActionsFullOperation() {
    $expectedResult = array(
      array(
        "id" => "mbp",
        "title"  => "MacBook Pro",
        "quantity" => 2,
        "price" => 29.99,
        "currency" => "EUR",
        "operation" => "add"
      ),
      array(
        "id" => "zen",
        "title"  => "Asus Zenbook",
        "quantity" => 3,
        "price" => 99.99,
        "currency" => "USD",
        "operation" => "add"
      ),
      array(
        "id" => "mbp",
        "title"  => "MacBook Pro",
        "quantity" => 5,
        "price" => 100.09,
        "currency" => "GBP",
        "operation" => "add"
      ),
      array(
        "id" => "zen",
        "title"  => "Asus Zenbook",
        "quantity" => -1,
        "price" => 99.99,
        "currency" => "USD",
        "operation" => "remove"
      ),
      array(
        "id" => "len",
        "title"  => "Lenovo P1",
        "quantity" => 8,
        "price" => 60.33,
        "currency" => "USD",
        "operation" => "add"
      ),
      array(
        "id" => "zen",
        "title"  => "Asus Zenbook",
        "quantity" => 1,
        "price" => 120.99,
        "currency" => "EUR",
        "operation" => "add"
      )
    );

    $actualResult = cartActionsParser(__DIR__ . "/data/testCartActionsFullOperation.txt");
    
    $this->assertEquals($expectedResult, $actualResult);
  }
}