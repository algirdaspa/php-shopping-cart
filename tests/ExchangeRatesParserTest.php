<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;

use function InvertusTask\ShoppingCart\utils\exchangeRatesParser;

class ExchangeRatesParserTest extends TestCase {
  function testExchangeRatesEmptyFile() {
    $expectedResult = "Empty exchange rates file or incorrect format!";
    $this->expectExceptionMessage($expectedResult);
    exchangeRatesParser(__DIR__ . "/data/testExchangeRatesEmptyFile.txt");
  }

  function testExchangeRatesIncorrectFormat() {
    $expectedResult = "Incorrect exchange rates format!";
    $this->expectExceptionMessage($expectedResult);
    exchangeRatesParser(__DIR__ . "/data/testExchangeRatesIncorrectFormat.txt");
  }

  function testExchangeRatesMissingCurrency() {
    $expectedResult = "Incorrect exchange rates format!";
    $this->expectExceptionMessage($expectedResult);
    exchangeRatesParser(__DIR__ . "/data/testExchangeRatesMissingCurrency.txt");
  }

  function testExchangeRatesFullOperation() {
    $expectedResult = array(
      "EUR" => array(
        "EUR" => 1,
        "USD" => 1.14,
        "GBP" => 0.88
      ),

      "USD" => array(
        "USD" => 1,
        "EUR" => 0.88,
        "GBP" => 0.74
      ),

      "GBP" => array(
        "GBP" => 1,
        "USD" => 1.36,
        "EUR" => 1.14
      )
    );

    $actualResult = exchangeRatesParser(__DIR__ . "/data/testExchangeRatesFullOperation.txt");
    
    $this->assertEquals($expectedResult, $actualResult);
  }
}