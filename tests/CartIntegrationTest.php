<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use InvertusTask\ShoppingCart\models\Cart;
use InvertusTask\ShoppingCart\models\Product;

use function InvertusTask\ShoppingCart\utils\cartActionsParser;

class CartIntegrationTest extends TestCase {
  function testCartTotalValue() {
    $cart = new Cart("EUR");
    $cartActions = cartActionsParser(__DIR__ . "/data/testCartActionsFullOperation.txt");

    foreach ($cartActions as $product) {
      if ($product["operation"] === "add") {
        $cart->addToCart(new Product($product["id"], $product["title"], $product["quantity"], $product["price"], $product["currency"]));
      } else {
        $cart->removeFromCart(new Product($product["id"], $product["title"], $product["quantity"], $product["price"], $product["currency"]));
      }
    }
    
    $expectedResult = "(18) 1352.17 EUR";
    $actualResult = $cart->returnCart();

    $this->assertEquals($expectedResult, $actualResult);
  }
}