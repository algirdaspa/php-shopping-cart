<?php

require_once __DIR__ . "/vendor/autoload.php";

use InvertusTask\ShoppingCart\models\Cart;
use InvertusTask\ShoppingCart\models\Product;

use function InvertusTask\ShoppingCart\utils\cartActionsParser;

$cart = new Cart("EUR");
$cartActions = cartActionsParser(__DIR__ . "/src/data/cartActions.txt");

foreach ($cartActions as $product) {
  if ($product["operation"] === "add") {
    $cart->addToCart(new Product($product["id"], $product["title"], $product["quantity"], $product["price"], $product["currency"]));
  } else {
    $cart->removeFromCart(new Product($product["id"], $product["title"], $product["quantity"], $product["price"], $product["currency"]));
  }

  print($cart->returnCart() . PHP_EOL);
}