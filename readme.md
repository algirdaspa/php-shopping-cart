# Shopping Cart Task

This repository is a shopping cart assignment task. The code base is fully functional and designed having usability in mind. All sub-tasks were accomplished and tests were written.

Technologies:

- PHP 7.4

- Composer 2.2.3

- PHPUnit 9.5.11

## Usage

To install the required dependencies, simply run:

```
composer install
```

To run tests, run predefined script:

```
composer test
```

To run the program, use predefined script:

```
composer start
```

## Documentation

### Program behavior

Running predefined start script basically reads actions file and returns cart state after each action in the CLI. Exchange rates file is parsed and the total cart value is recalculated after every user action in order to imitate a real use case behavior, when the currency exchange rate may be updated, therefore, the sum is shown to the user according to the latest information. While this may look like a resource wasting, it was done on purpose.

### Actions

In the code base, action is used to describe user interaction with cart, for example, adding to cart is an action, removing from cart is also an action. Actions can be defined in the cartActions.txt file, which is located at "data" folder in "src" directory. Each action should be specified in a new line and values must be separated by a semicolon. Action declaration is sensitive, which means, that if semicolon is not placed at the correct position or if not all values are provided, then the action is ignored.

Action contains such data as product id, title, quantity, price and currency. If the provided quantity is positive, it indicates an add to cart action, otherwise, if quantity is negative, it is assumed that remove from cart action is taken. An example of how an action should be specified:

```
zen;Asus Zenbook;1;120.99;EUR;
```

### Currencies

Currency exchange rates are specified in the exchangeRates.txt file, which is located at "data" folder in "src" directory. It is easy to add additional currencies without editing the actual code base. However, currency declaration is a critical part of an application, therefore, in order for the program to work, currencies must be specified in the required format. If the format of currencies is incorrect, an exception will be raised.

Currencies are parsed line by line, therefore, relevant exchange rates for a specific currency should be specified in one line. Each value of the line should be separated by a semicolon. First goes the currency short code and after a semicolon goes exchange rate with semicolon at the end. First currency on a line should be the "host" currency and its exchange rate should be exactly 1.

If adding additional currencies, exchange rate should be specified for all the other currencies, otherwise, an exception will be thrown. An example of additional currency added to the file:

```
EUR;1;USD;1.14;GBP;0.88;JPY;131.37;
USD;1;EUR;0.88;GBP;0.74;JPY;115.91;
GBP;1;USD;1.36;EUR;1.14;JPY;157.32;
JPY;1;GBP;0.0064;EUR;0.0076;USD;0.0086;
```